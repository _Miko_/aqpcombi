﻿using System;

namespace ArequipaCombi
{
    using System.Threading;
    using Android.App;
    using Android.Content.PM;
    using Android.OS;
    using Android.Widget;

    [Activity(Theme = "@style/MyTheme.Splash", MainLauncher = true, Icon = "@drawable/logoarequipacombi", ScreenOrientation = ScreenOrientation.Portrait, NoHistory = true)]
    public class SplashScreen : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            try
            {
                base.OnCreate(bundle);
                Thread.Sleep(100);
                StartActivity(typeof(CombiMenuActivity));
            }
            catch (Exception)
            {
                Toast.MakeText(this, "conexión de internet fallida", ToastLength.Long).Show();
            }
        }
    }
}