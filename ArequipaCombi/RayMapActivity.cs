using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ArequipaCombi.Core;
using Java.Util;
using Plugin.Geolocator;
using Android.Gms.Ads;
using Acr.UserDialogs;

namespace ArequipaCombi
{
    [Activity(Label = "Ruta de la Combi y tu ubicaci�n")]
    public class RayMapActivity : Activity
    {
        private MapFragment _mapfragment;
        private GoogleMap _googlemap;
        private LatLng _raylocation;
        private Task _t;
        AdView _madview;

        readonly string[] PermissionsLocation =
        {
          Manifest.Permission.AccessCoarseLocation,
          Manifest.Permission.AccessFineLocation
        };

        const int REQUESTLOCATIONID = 0;
 
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.RayMapView);
            Android.Gms.Ads.MobileAds.Initialize(this, "ca-app-pub-3930203460679732~7340184304");
            UserDialogs.Init(this);
            RutaRepository dataServices = new RutaRepository();
            ActionBar.Hide();
            _t = dataServices.RutaConvertir();
            CreateMapFragment();
            TryGetLocationAsync();
            ShowAdsBanner();
        }

        private async Task Waiting()
        {
            try
            {
                using (var objDialog = UserDialogs.Instance.Loading("Cargando", null, null, true, MaskType.Black))
                {
                    await Localizar();
                }
            }
            catch (Exception)
            {
                throw;
            } 
        }

        void ShowAdsBanner()
        {
            try
            {
                _madview = FindViewById<AdView>(Resource.Id.adView);
                var adRequest = new AdRequest.Builder().Build();
                _madview.LoadAd(adRequest);
            }
            catch (Exception)
            {
                throw;
            }
        }

        async Task TryGetLocationAsync()
        {
            try
            {
                if ((int)Build.VERSION.SdkInt < 23)
                {
                    Waiting();
                    return;
                }

                await GetLocationPermissionAsync();
            }
            catch (Exception)
            {

                throw;
            }
        }

        async Task GetLocationPermissionAsync()
        {
            try
            {
                const string permission = Manifest.Permission.AccessFineLocation;
                if (CheckSelfPermission(permission) == (int)Permission.Granted)
                {
                    Waiting();
                    return;
                }

                
                if (ShouldShowRequestPermissionRationale(permission))
                {
                    RequestPermissions(PermissionsLocation, REQUESTLOCATIONID);

                    return;

                }
                //Finally request permissions with the list of permissions and Id
                RequestPermissions(PermissionsLocation, REQUESTLOCATIONID);
            }
            catch (Exception)
            {
                throw;
            }      
        }

        public override async void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            try
            {
                switch (requestCode)
                {
                    case REQUESTLOCATIONID:
                        {
                            if (grantResults[0] == Permission.Granted)
                            {
                                //Permission granted
                                await Waiting();
                            }
                            else
                            {
                                //Permission Denied :(
                                //Disabling location functionality
                                Toast.MakeText(this, "gps o internet desactivado, a�n no podemos adivinar las rutas de combis =(", ToastLength.Long).Show();
                            }
                        }
                        break;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void UpdateMapView(double lat, double lon)
        {
            try
            {
                _raylocation = new LatLng(lat, lon);

                var mapReadyCallback = new LocalMapReady();

                mapReadyCallback.MapReady += (sender, args) =>
                {
                    _googlemap = (sender as LocalMapReady).Map;

                    if (_googlemap != null)
                    {
                        MarkerOptions markerOptions = new MarkerOptions();
                        markerOptions.SetPosition(_raylocation);
                        markerOptions.SetTitle("Lugar actual");
                        _googlemap.AddMarker(markerOptions);
                        CameraUpdate cameraUpdate = CameraUpdateFactory.NewLatLngZoom(_raylocation, 15);
                        _googlemap.MoveCamera(cameraUpdate);
                        _googlemap.SetMinZoomPreference(11);
                        _googlemap.SetMaxZoomPreference(17);
                        if (ArequipaCombi.Core.RutaRepository._persistenciaurl != ArequipaCombi.Core.RutaRepository._urlapoyo)
                        {
                            Esperar();
                        }
                        else
                        {
                            _googlemap.AddPolyline(dibujar());
                        }
                    }
                };

                _mapfragment.GetMapAsync(mapReadyCallback);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void CreateMapFragment()
        {
            try
            {
                _mapfragment = FragmentManager.FindFragmentByTag("map") as MapFragment;

                if (_mapfragment == null)
                {
                    var googleMapOptions = new GoogleMapOptions()
                        .InvokeMapType(GoogleMap.MapTypeNormal)
                        .InvokeZoomControlsEnabled(true)

                        .InvokeCompassEnabled(true);


                    FragmentTransaction fragmentTransaction = FragmentManager.BeginTransaction();
                    _mapfragment = MapFragment.NewInstance(googleMapOptions);
                    fragmentTransaction.Add(Resource.Id.mapFrameLayout, _mapfragment, "map");
                    fragmentTransaction.Commit();
                }
            }
            catch (Exception)
            {
                throw;
            }         
        }

        private async void Esperar()
        {
            try
            {
                await _t;
                _googlemap.AddPolyline(dibujar());
            }
            catch (Exception)
            {
                throw;
            } 
        }

        private async Task Localizar()        
        {
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 100;
                TimeSpan ts = TimeSpan.FromMilliseconds(20000);
                var loc = await locator.GetPositionAsync(ts);
                UpdateMapView(loc.Latitude, loc.Longitude);
                
                
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, "Error, Permita el uso de gps", ToastLength.Long).Show();
            }
        }

        private PolylineOptions dibujar()
        {
            try
            {
                PolylineOptions ruta = new PolylineOptions();
                for (int j = 0; j < ArequipaCombi.Core.RutaRepository._rutagrupo[0].Ruta.Count; j++)
                {
                    ruta.Add(new LatLng(ArequipaCombi.Core.RutaRepository._rutagrupo[0].Ruta[j], ArequipaCombi.Core.RutaRepository._rutagrupo[0].Ruta[j + 1]));
                    j = j + 1;
                }
                return ruta;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private class LocalMapReady : Java.Lang.Object, IOnMapReadyCallback
        {
            public GoogleMap Map { get; private set; }

            public event EventHandler MapReady;

            public void OnMapReady(GoogleMap googleMap)
            {
                Map = googleMap;
                var handler = MapReady;
                if (handler != null)
                    handler(this, EventArgs.Empty);
            }
        }
    }
}