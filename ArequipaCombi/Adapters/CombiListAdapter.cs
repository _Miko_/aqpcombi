using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ArequipaCombi.Core;

namespace ArequipaCombi.Adapters
{
    public class CombiListAdapter : BaseAdapter<Combi>
    {
        List<Combi> items;
        Activity context;

        public CombiListAdapter(Activity context, List<Combi> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Combi this[int position]
        {
            get
            {
                return items[position];
            }
        }

        public override int Count
        {
            get
            {
                return items.Count;
            }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            try
            {
                var item = items[position];

                if (convertView == null)
                {
                    convertView = context.LayoutInflater.Inflate(Resource.Layout.CombiRowView, null);
                }
                var resourceId = (int)typeof(Resource.Drawable).GetField(item.Imagen).GetValue(null);

                convertView.FindViewById<TextView>(Resource.Id.hotDogNameTextView).SetBackgroundResource(resourceId);

                return convertView;
            }
            catch (Exception)
            {
                return convertView;
            }
            
        }
    }
}