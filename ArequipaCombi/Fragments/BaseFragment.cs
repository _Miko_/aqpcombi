using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using ArequipaCombi.Core;
using ArequipaCombi.Core.Service;

namespace ArequipaCombi.Fragments
{
    public class BaseFragment : Fragment
    {
        // 1 = cerrocolorado
        // 2 = joseluisbustamante
        // 3 = miraflores
        // 4 = socabaya
        // 5 = yanahuara
        
        protected ListView listView;
        protected CombiDataService hotDogDataService;
        protected List<Combi> hotDogs;

        public BaseFragment()
        {
            hotDogDataService = new CombiDataService();
        }

        protected void HandleEvents()
        {
            listView.ItemClick += ListView_ItemClick;
        }
        protected void FindViews()
        {
            listView = this.View.FindViewById<ListView>(Resource.Id.hotDogListView);
        }

        protected void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            try
            {
                var hotDog = hotDogs[e.Position];

                var intent = new Intent();
                intent.SetClass(this.Activity, typeof(CombiDetailActivity));
                intent.PutExtra("selectedHotDogId", hotDog.CombiId);

                StartActivityForResult(intent, 100);
            }
            catch (Exception)
            {
                throw;
            }
           
        }
    }
}