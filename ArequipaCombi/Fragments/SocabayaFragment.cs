﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using ArequipaCombi.Adapters;

namespace ArequipaCombi.Fragments
{
    public class SocabayaFragment : BaseFragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            FindViews();
    
            HandleEvents();

            hotDogs = hotDogDataService.GetHotDogsForGroup(4);
            listView.Adapter = new CombiListAdapter(this.Activity, hotDogs);
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.SocabayaFragment, container, false);
        }
    }
}