using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ArequipaCombi.Core;
using ArequipaCombi.Core.Service;

namespace ArequipaCombi
{
    [Activity(Label = "Detalles de la Combi")]
    public class CombiDetailActivity : Activity
    {
        private Button externalMapButton;
        private ImageView CombiImageView;
        private TextView CombiTextView;
        private TextView CombiDescTotalTextView;
        private TextView PrecioTextView;
        private int combiseleccionada;
    

        private Combi selectedCombi;
        private CombiDataService dataService;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.CombiDetailView);

            CombiDataService dataService = new CombiDataService();
            ActionBar.Hide();
            var selectedHotDogId = Intent.Extras.GetInt("selectedHotDogId");
            selectedCombi = dataService.GetCombiById(selectedHotDogId);

            FindViews();

            BindData();

            HandleEvents();
        }

        private void FindViews()
        {
            try
            {
                CombiTextView = FindViewById<TextView>(Resource.Id.CombiTextView);
                CombiDescTotalTextView = FindViewById<TextView>(Resource.Id.CombiDescTotalTextView);
                PrecioTextView = FindViewById<TextView>(Resource.Id.PrecioTextView);
                externalMapButton = FindViewById<Button>(Resource.Id.externalMapButton);
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        private void BindData()
        {
            try
            {
                CombiTextView.Text = selectedCombi.Name;
                CombiDescTotalTextView.Text = selectedCombi.Description;
                //PrecioTextView.Text = "Precio del pasaje: " + selectedCombi.Price;
                ArequipaCombi.Core.CombiRepository._combiseleccion = selectedCombi.CombiId; 
                ArequipaCombi.Core.RutaRepository._persistenciaurl = selectedCombi.Url; 
                //var resourceId2 = (int)typeof(Resource.Drawable).GetField(selectedCombi.Imagen).GetValue(null);
                //CombiImageView.SetImageResource(resourceId2);
            }
            catch (Exception)
            {
                throw;
            }
            

        }
        private void HandleEvents()
        {
            externalMapButton.Click += externalMapButton_Click;
        }
        private void externalMapButton_Click(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(RayMapActivity));
            StartActivity(intent);
        }
    }
}