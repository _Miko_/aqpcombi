using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ArequipaCombi.Adapters;
using ArequipaCombi.Core;
using ArequipaCombi.Core.Service;
using ArequipaCombi.Fragments;

namespace ArequipaCombi
{
    [Activity(Label = "Combi seg�n distrito que recorre")]
    public class CombiMenuActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.CombiMenuView);

            ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;
            AddTab("Cerro Colorado", new CerrocoloradoFragment());
            AddTab("Jose Luis B.", new JoseluisbustamanteFragment());
            AddTab("Miraflores", new MirafloresFragment());
            AddTab("Socabaya", new SocabayaFragment());
            AddTab("Yanahuara", new YanahuaraFragment());   
        }

        private void AddTab(string tabText, Fragment view)
        {
            try
            {
                var tab = this.ActionBar.NewTab();
                tab.SetText(tabText);

                tab.TabSelected += delegate (object sender, ActionBar.TabEventArgs e)
                {
                    var fragment = this.FragmentManager.FindFragmentById(Resource.Id.fragmentContainer);
                    if (fragment != null)
                        e.FragmentTransaction.Remove(fragment);
                    e.FragmentTransaction.Add(Resource.Id.fragmentContainer, view);
                };

                tab.TabUnselected += delegate (object sender, ActionBar.TabEventArgs e)
                {
                    e.FragmentTransaction.Remove(view);
                };

                this.ActionBar.AddTab(tab);
            }
            catch (Exception)
            {

                throw;
            }
            
        }
    }
}